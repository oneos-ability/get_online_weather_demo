# get_online_weather_demo

#### 介绍
丐版基于TCP连接获取在线天气的demo，仅提供思路，请用户根据实际需求修改使用。

#### 使用说明

1.  拷贝工程文件夹至project工程目录
2.  keil编译烧录
3.  命令行使用``get_weather_demo``运行demo

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request