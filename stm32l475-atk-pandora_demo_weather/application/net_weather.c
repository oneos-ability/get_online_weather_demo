/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        net_weather.c
 *
 * @brief       simple online weather info getting demo
 *
 * @revision
 * Date         Author          Notes
 * 2023-10-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <os_errno.h>
#include <os_assert.h>
#include <sys/socket.h>

/* 心知天气（www.seniverse.com）的 API 目前基于 http 协议，端口是缺省的 80(http)和 443(https) */
#define WEATHER_PORT          (80)
#define DATA_MAX_BUFF_SIZE    (32)
#define PACKAGE_MAX_BUFF_SIZE (300)
#define WEATHER_IP_ADDR       "116.62.81.138"
#define WEATHER_SITE_KEY      "SiSdJhCTQEz-xxxxx"
#define TEMPERATURE_FLAG      "temperature\":\""
#define DEMO_DNS

/* 构造的GET请求包 */
#define GET_REQUEST_PACKAGE_HTTPS                                                                                      \
    "GET https://api.seniverse.com/v3/weather/%s.json?key=%s&location=%s&language=zh-Hans&unit=c\r\n\r\n"

#define GET_REQUEST_PACKAGE_HTTP                                                                                       \
    "GET http://api.seniverse.com/v3/weather/now.json?key=%s&location=%s&language=zh-Hans&unit=c\r\n\r\n"

/* 预留实况天气数据结构体 非付费版按需增减 */
typedef struct
{
    char id[DATA_MAX_BUFF_SIZE];                 // id
    char name[DATA_MAX_BUFF_SIZE];               // 地名
    char country[DATA_MAX_BUFF_SIZE];            // 国家
    char path[DATA_MAX_BUFF_SIZE];               // 完整地名路径
    char timezone[DATA_MAX_BUFF_SIZE];           // 时区
    char timezone_offset[DATA_MAX_BUFF_SIZE];    // 时差
    char text[DATA_MAX_BUFF_SIZE];               // 天气预报文字
    char code[DATA_MAX_BUFF_SIZE];               // 天气预报代码
    char temperature[DATA_MAX_BUFF_SIZE];        // 气温
    char last_update[DATA_MAX_BUFF_SIZE];        // 最后一次更新的时间
} Weather;

static int get_weather_from_net(char *location, Weather *result);

/*******************************************************************************************************
** 函数: get_weather_main，网络天气获取demo主函数
**------------------------------------------------------------------------------------------------------
** hints:
** 1st step: get data from server
** 2nd step: parse data eg.cjson
** 3nd step: show weather
********************************************************************************************************/
int get_weather_main(int argc, char *argv[])
{
    int     ret          = 0;
    char   *location     = "chengdu";
    Weather weather_data = {0};

    printf("[%s-%d] ---------------------get weather demo start---------------------\r\n", __func__, __LINE__);

    ret = get_weather_from_net(location, &weather_data);

    printf("[%s-%d] ---------------------get weather demo exit:%d---------------------\r\n", __func__, __LINE__, ret);

    return ret;
}

/*******************************************************************************************************
** 函数: get_weather_from_net，基于TCP连接服务器获取天气信息，并简单解析打印当前气温
**------------------------------------------------------------------------------------------------------
** 参数: location：城市名   result：预留参数，保留数据解析的结果
** 返回: int：执行结果
********************************************************************************************************/
static int get_weather_from_net(char *location, Weather *result)
{
    int                  fd;
    char                *str_ptr                         = OS_NULL;
    int                  ret                             = 0;
    struct sockaddr_in   addr                            = {0};
    char                 send_buf[PACKAGE_MAX_BUFF_SIZE] = {0};
    char                 recv_buf[PACKAGE_MAX_BUFF_SIZE] = {0};
    const struct timeval recv_time                       = {.tv_sec = 20, .tv_usec = 0};

#ifndef DEMO_DNS
    addr.sin_family      = AF_INET;
    addr.sin_port        = htons(WEATHER_PORT);
    addr.sin_addr.s_addr = inet_addr(WEATHER_IP_ADDR);
#else
    struct hostent *host = gethostbyname("api.seniverse.com");
    OS_ASSERT(OS_NULL != host);

    addr.sin_family = AF_INET;
    addr.sin_port   = htons(WEATHER_PORT);
    addr.sin_addr   = *((struct in_addr *)host->h_addr);
    memset(&(addr.sin_zero), 0, sizeof(addr.sin_zero));
#endif

    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (0 > fd)
    {
        printf("[%s-%d] socket create error\r\n", __func__, __LINE__);
        return fd;
    }

    ret = connect(fd, (struct sockaddr *)&addr, sizeof(addr));
    if (0 > ret)
    {
        printf("[%s-%d] socket connect error\r\n", __func__, __LINE__);
        ret = closesocket(fd);
        return ret;
    }

    ret = setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &recv_time, sizeof(recv_time));
    OS_ASSERT(0 == ret);

    /* 组合GET请求包 ps.unsafe */
    sprintf(send_buf, GET_REQUEST_PACKAGE_HTTP, WEATHER_SITE_KEY, location);

    /* 发送数据到服务端 */
    ret = send(fd, (const void *)send_buf, strlen(send_buf), 0);
    OS_ASSERT(strlen(send_buf) == ret);

    /* 接收天气数据简单示例 */
    ret = recv(fd, recv_buf, PACKAGE_MAX_BUFF_SIZE - 1, 0);
    printf("[%s-%d] socket recv %d bytes data %s\r\n", __func__, __LINE__, ret, recv_buf);

    closesocket(fd);

    /* 简单解析气温并显示示例 */
    str_ptr = strstr(recv_buf, TEMPERATURE_FLAG);
    if (OS_NULL != str_ptr)
    {
        str_ptr += strlen(TEMPERATURE_FLAG);
        printf("[%s-%d] get weather of %s[%d]\r\n", __func__, __LINE__, location, atoi(str_ptr));
        ret = OS_SUCCESS;
    }
    else
    {
        ret = OS_FAILURE;
    }

    return ret;
}

#ifdef OS_USING_SHELL
#include <shell.h>
SH_CMD_EXPORT(get_weather_demo, get_weather_main, "get_online weather_info demo");
#endif /* OS_USING_SHELL */

#if 0 // 附cJSON解析参考
/*******************************************************************************************************
** 函数: cJSON_NowWeatherParse，解析天气实况数据
**------------------------------------------------------------------------------------------------------
** 参数: JSON：天气数据包   result：数据解析的结果
** 返回: void
********************************************************************************************************/
static int cJSON_NowWeatherParse(char *JSON, Weather *result)
{
    cJSON *json, *arrayItem, *object, *subobject, *item;

    json = cJSON_Parse(JSON);    // 解析JSON数据包
    if (json == NULL)            // 检测JSON数据包是否存在语法上的错误，返回NULL表示数据包无效
    {
        printf("Error before: [%s]\n", cJSON_GetErrorPtr());    // 打印数据包语法错误的位置
        return 1;
    }
    else
    {
        if ((arrayItem = cJSON_GetObjectItem(json, "results")) != NULL)
            ;    // 匹配字符串"results",获取数组内容
        {
            int size = cJSON_GetArraySize(arrayItem);    // 获取数组中对象个数
#if DEBUG
            printf("cJSON_GetArraySize: size=%d\n", size);
#endif
            if ((object = cJSON_GetArrayItem(arrayItem, 0)) != NULL)    // 获取父对象内容
            {
                /* 匹配子对象1：城市地区相关 */
                if ((subobject = cJSON_GetObjectItem(object, "location")) != NULL)
                {
                    // 匹配id
                    if ((item = cJSON_GetObjectItem(subobject, "id")) != NULL)
                    {
                        memcpy(result->id, item->valuestring, strlen(item->valuestring));    // 保存数据供外部调用
                    }
                    // 匹配城市名
                    if ((item = cJSON_GetObjectItem(subobject, "name")) != NULL)
                    {
                        memcpy(result->name, item->valuestring, strlen(item->valuestring));    // 保存数据供外部调用
                    }
                    // 匹配城市所在的国家
                    if ((item = cJSON_GetObjectItem(subobject, "country")) != NULL)
                    {
                        memcpy(result->country, item->valuestring, strlen(item->valuestring));    //
                        保存数据供外部调用
                    }
                    // 匹配完整地名路径
                    if ((item = cJSON_GetObjectItem(subobject, "path")) != NULL)
                    {
                        memcpy(result->path, item->valuestring, strlen(item->valuestring));    // 保存数据供外部调用
                    }
                    // 匹配时区
                    if ((item = cJSON_GetObjectItem(subobject, "timezone")) != NULL)
                    {
                        memcpy(result->timezone, item->valuestring, strlen(item->valuestring));    //
                        保存数据供外部调用
                    }
                    // 匹配时差
                    if ((item = cJSON_GetObjectItem(subobject, "timezone_offset")) != NULL)
                    {
                        memcpy(result->timezone_offset,
                               item->valuestring,
                               strlen(item->valuestring));    // 保存数据供外部调用
                    }
                }
                /* 匹配子对象2：今天的天气情况 */
                if ((subobject = cJSON_GetObjectItem(object, "now")) != NULL)
                {
                    // 匹配天气现象文字
                    if ((item = cJSON_GetObjectItem(subobject, "text")) != NULL)
                    {
                        memcpy(result->text, item->valuestring, strlen(item->valuestring));    // 保存数据供外部调用
                    }
                    // 匹配天气现象代码
                    if ((item = cJSON_GetObjectItem(subobject, "code")) != NULL)
                    {
                        memcpy(result->code, item->valuestring, strlen(item->valuestring));    // 保存数据供外部调用
                    }
                    // 匹配气温
                    if ((item = cJSON_GetObjectItem(subobject, "temperature")) != NULL)
                    {
                        memcpy(result->temperature,
                               item->valuestring,
                               strlen(item->valuestring));    // 保存数据供外部调用
                    }
                }
                /* 匹配子对象3：数据更新时间（该城市的本地时间） */
                if ((subobject = cJSON_GetObjectItem(object, "last_update")) != NULL)
                {
                    memcpy(result->last_update,
                           subobject->valuestring,
                           strlen(subobject->valuestring));    // 保存数据供外部调用
                }
            }
        }
    }

    cJSON_Delete(json);    // 释放cJSON_Parse()分配出来的内存空间

    return 0;
}
#endif
